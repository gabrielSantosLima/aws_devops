const express = require("express");

const app = express();

app.get("/", (req, resp) => {
  return resp.json({
    hello: "world",
    todayis: new Date().toISOString(),
  });
});

app.listen(3333);
